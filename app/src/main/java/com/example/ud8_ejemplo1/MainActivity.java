package com.example.ud8_ejemplo1;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;


import com.example.ud8_ejemplo1.basedatos.Trabajador;
import com.example.ud8_ejemplo1.viewmodel.TrabajadorViewModel;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

public class MainActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private TrabajadorAdapter trabajadorAdapter;
    private TrabajadorViewModel trabajadorViewModel;
    public int posisicionPulsada;

    ActivityResultLauncher<Intent> resultInsertar = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>() {
                @Override
                public void onActivityResult(ActivityResult result) {
                    // Comprobamos que el código de respuesta es correcto
                    if (result.getResultCode() == Activity.RESULT_OK) {
                        // Obtenemos el Intent devuelto
                        Intent datosResult = result.getData();

                        Trabajador trabajador = new Trabajador(datosResult.getStringExtra("trabajador"));
                        trabajadorViewModel.insertar(trabajador);
                    }
                }
            });

    ActivityResultLauncher<Intent> resultActualizar = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>() {
                @Override
                public void onActivityResult(ActivityResult result) {
                    // Comprobamos que el código de respuesta es correcto
                    if (result.getResultCode() == Activity.RESULT_OK) {
                        // Obtenemos el Intent devuelto
                        Intent datosResult = result.getData();

                        trabajadorViewModel.actualizar(datosResult.getIntExtra("id", 0), datosResult.getStringExtra("nombre"));
                    }
                }
            });

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.recyclerview);

        recyclerView.setHasFixedSize(true);

        recyclerView.addItemDecoration(new DividerItemDecoration(MainActivity.this, 1));

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(MainActivity.this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);

        trabajadorAdapter = new TrabajadorAdapter(this);

        trabajadorAdapter.setOnItemClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Obtenemos la posición pulsada del recyclerView para actualizarla o eliminarla.
                posisicionPulsada = recyclerView.getChildAdapterPosition(v);

                Trabajador t = trabajadorAdapter.lista.get(posisicionPulsada);

                Intent intent = new Intent(MainActivity.this, ActualizarTrabajador.class);

                intent.putExtra("id", t.getId());
                intent.putExtra("nombre", t.getNombre());

                resultActualizar.launch(intent);
            }
        });

        recyclerView.setAdapter(trabajadorAdapter);

        // Obtenemos uno nuevo o ya existente ViewModel desde la clase ViewModelProviders.
        trabajadorViewModel = new ViewModelProvider(this).get(TrabajadorViewModel.class);

        // Añadimos un observador al LiveData devuelto por el método obtenerTodosTrabajadores.
        // el método onchanged se ejecuta cuando el observador detecta un cambio y la actividad está
        // en segundo plano.
        trabajadorViewModel.obtenerTodosTrabajadores().observe(this, new Observer<List<Trabajador>>() {
            @Override
            public void onChanged(@Nullable final List<Trabajador> trabajadores) {
                // Actualizamos el cambio en el RecyclerView.
                trabajadorAdapter.anyadirALista(trabajadores);
            }
        });

        // Buscamos el FAB y configuramos su onClickListener
        FloatingActionButton fab = findViewById(R.id.fab);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, InsertarTrabajador.class);

                resultInsertar.launch(intent);
            }
        });
    }
}